import { Injectable } from '@angular/core';
import { UserSummary } from '../models';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, map } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/compat/database';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  users$: Observable<UserSummary[]>;

  constructor(private _snackBar: MatSnackBar, private db: AngularFireDatabase) {
    this.users$ = this.db
      .list(`/korisnici`)
      .snapshotChanges()
      .pipe(
        map((changes: any[]) => {
          return changes.map((c) => {
            return Object.assign({}, { id: c.payload.key }, c.payload.val());
          });
        })
      );
  }

  deleteUser(key: string) {
    this.db.list('/korisnici').remove(key);
    this.openSnackBar('Korisnički nalog uspešno izbrisan.');
  }

  addUser(user: UserSummary) {
    this.db.list('/korisnici').push(user);

    this.openSnackBar('Korisnički nalog uspešno kreiran.');
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, '🐾', {
      duration: 2500,
    });
  }

  editUser(user: UserSummary) {
    this.db.list(`/korisnici`).update(user.id!, user);

    this.openSnackBar('Podaci o korisničkom nalogu uspešno izmenjeni.');
  }

  selectUserById(userId: string) {
    return this.db.object(`/korisnici/${userId}`).valueChanges();
  }
}
