import { Injectable } from '@angular/core';
import {
  AccountEditorComponent,
  ConfirmationDialogComponent,
  LoginDialogComponent,
  ProductEditorComponent,
  StoreEditorComponent,
} from '../components';
import { MatDialog } from '@angular/material/dialog';
import { PetShop, Product, User, UserSummary } from '../models';
import { ProductService } from './product.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  defaultEnterAnimationDuration = '500ms';
  defaultExitAnimationDuration = '200ms';
  users: UserSummary[] = [];

  constructor(
    private dialog: MatDialog,
    private userService: UserService,
    private productService: ProductService
  ) {
    userService.users$.subscribe((items) => {
      this.users = items;
    });
  }

  login() {
    this.dialog
      .open(LoginDialogComponent, {
        width: '400px',
        enterAnimationDuration: this.defaultEnterAnimationDuration,
        exitAnimationDuration: this.defaultExitAnimationDuration,
      })
      .afterClosed()
      .subscribe((userData) => {
        console.log('LOGIN', userData, this.users);
        const user: UserSummary | undefined = this.users.find((user) => {
          return (
            user.korisnickoIme === userData.username &&
            user.lozinka === userData.password
          );
        });
        if (!user) {
          alert('Uneli ste pogresne kredencijale.');
        }
      });
  }

  removeItemFromCartConfirmation() {
    return this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      enterAnimationDuration: this.defaultEnterAnimationDuration,
      exitAnimationDuration: this.defaultExitAnimationDuration,
      data: {
        title: 'Potvrda uklanjanja proizvoda',
        message: 'Da li ste sigurni da želite da uklonite proizvod iz korpe?',
      },
    });
  }

  deleteAccountConfirmation() {
    return this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      enterAnimationDuration: this.defaultEnterAnimationDuration,
      exitAnimationDuration: this.defaultExitAnimationDuration,
      data: {
        title: 'Potvrda brisanja naloga',
        message: 'Da li ste sigurni da želite da izbrišete korisnički nalog?',
      },
    });
  }

  deleteProductConfirmation() {
    return this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      enterAnimationDuration: this.defaultEnterAnimationDuration,
      exitAnimationDuration: this.defaultExitAnimationDuration,
      data: {
        title: 'Potvrda brisanja proizvoda',
        message: 'Da li ste sigurni da želite da izbrišete proizvod?',
      },
    });
  }

  editStore(store: PetShop) {
    return this.dialog.open(StoreEditorComponent, {
      width: '500px',
      enterAnimationDuration: this.defaultEnterAnimationDuration,
      exitAnimationDuration: this.defaultExitAnimationDuration,
      data: { item: store },
    });
  }

  createAccount(creator: 'user' | 'admin') {
    this.dialog
      .open(AccountEditorComponent, {
        width: '500px',
        enterAnimationDuration: this.defaultEnterAnimationDuration,
        exitAnimationDuration: this.defaultExitAnimationDuration,
        data: { creator: creator, title: 'Kreiranje korisničkog naloga' },
      })
      .afterClosed()
      .subscribe((user) => {
        this.userService.addUser(user);
      });
  }

  editAccount(user: User) {
    return this.dialog.open(AccountEditorComponent, {
      width: '500px',
      enterAnimationDuration: this.defaultEnterAnimationDuration,
      exitAnimationDuration: this.defaultExitAnimationDuration,
      data: { item: user, title: 'Izmena podataka o korisniku' },
    });
  }

  createProduct() {
    this.dialog
      .open(ProductEditorComponent, {
        width: '500px',
        enterAnimationDuration: this.defaultEnterAnimationDuration,
        exitAnimationDuration: this.defaultExitAnimationDuration,
        data: { title: 'Dodaj proizvod' },
      })
      .afterClosed()
      .subscribe((product) => {
        this.productService.addProduct(product);
      });
  }

  editProduct(product: Product) {
    return this.dialog.open(ProductEditorComponent, {
      width: '500px',
      enterAnimationDuration: this.defaultEnterAnimationDuration,
      exitAnimationDuration: this.defaultExitAnimationDuration,
      data: { item: product, title: 'Izmena podataka o proizvodu' },
    });
  }
}
