import { Injectable } from '@angular/core';
import { PetShop } from '../models';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, Subscription, map } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/compat/database';

@Injectable({
  providedIn: 'root',
})
export class PetShopService {
  petShop$!: Observable<any>;
  petShop!: any;

  constructor(private _snackBar: MatSnackBar, private db: AngularFireDatabase) {
    this.fetchStoreData();
  }

  fetchStoreData() {
    this.petShop$ = this.db.object('petShop').valueChanges();
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, '🐾', {
      duration: 2500,
    });
  }

  editPetShop(updatedShop: PetShop) {
    this.db.object('petShop').set(Object.assign({}, this.petShop, updatedShop));
    this.openSnackBar('Podaci o prodavnici uspešno izmenjeni.');
  }
}
