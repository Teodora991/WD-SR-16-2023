import { Injectable } from '@angular/core';
import { Product, ProductSummary } from '../models';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Observable, map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private dbPath = '/proizvodi';
  productItems$: Observable<ProductSummary[]>;

  constructor(private _snackBar: MatSnackBar, private db: AngularFireDatabase) {
    this.productItems$ = this.db
      .list(`${this.dbPath}/-MNVEu6iMr2EFlQO6TW60/`)
      .snapshotChanges()
      .pipe(
        map((changes: any[]) => {
          return changes.map((c) => {
            return Object.assign({}, { id: c.payload.key }, c.payload.val());
          });
        })
      );
  }

  selectProductById(productId: string) {
    return this.db
      .object(`${this.dbPath}/-MNVEu6iMr2EFlQO6TW60/${productId}`)
      .valueChanges();
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, '🐾', {
      duration: 2500,
    });
  }

  addProductToCart(productId: string) {
    this.patchProduct(productId, { uKorpi: true });
  }

  patchProduct(key: string, data: any) {
    this.db.list(`${this.dbPath}/-MNVEu6iMr2EFlQO6TW60/`).update(key, data);
  }

  removeProductFromCart(productId: string) {
    this.patchProduct(productId, { uKorpi: false });
  }

  deleteProduct(key: string) {
    this.db.list(`${this.dbPath}/-MNVEu6iMr2EFlQO6TW60/`).remove(key);

    this.openSnackBar('Proizvod uspešno izbrisan.');
  }

  addProduct(product: Product) {
    this.db.list(`${this.dbPath}/-MNVEu6iMr2EFlQO6TW60/`).push(product);
    this.openSnackBar('Proizvod uspešno dodat.');
  }

  editProduct(updatedProduct: ProductSummary) {
    this.patchProduct(updatedProduct.id!, updatedProduct);
  }
}
