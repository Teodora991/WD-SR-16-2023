export interface PetShopSummary {
  naziv?: string;
  logo?: string;
  adresa?: string;
  godinaOtvaranja?: string;
  telefon?: string;
}

export class PetShop implements PetShopSummary {}
