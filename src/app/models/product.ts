export interface ProductSummary {
  id?: string;
  naziv?: string;
  slike?: string[];
  kratakOpis?: string;
  detaljanOpis?: string;
  cena?: number;
  tip?: string;
  prosecnaOcena?: number;
  uKorpi?: boolean;
  ocene?: number[];
}

export class Product implements ProductSummary {}
