export interface UserSummary {
  korisnickoIme?: string;
  lozinka?: string;
  email?: string;
  ime?: string;
  prezime?: string;
  datumRodjenja?: string;
  adresa?: string;
  telefon?: string;
  id?: string;
}

export class User implements UserSummary {}
