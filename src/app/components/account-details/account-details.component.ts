import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogService, UserService } from '../../services';
import { User, UserSummary } from '../../models';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogActions } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';

@Component({
  selector: 'app-account-details',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogActions,
    MatDividerModule,
    MatCardModule,
  ],
  templateUrl: './account-details.component.html',
  styleUrl: './account-details.component.scss',
})
export class AccountDetailsComponent {
  activeRoute$: Subscription;
  userData: any = new User();
  selectedId: string | null = null;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private dialogService: DialogService
  ) {
    this.activeRoute$ = this.route.params.subscribe((params) => {
      this.selectedId = params['id'];
      this.userService.selectUserById(this.selectedId!).subscribe((user) => {
        this.userData = user;
      });
    });
  }

  ngOnDestroy() {
    this.activeRoute$?.unsubscribe();
  }
}
