import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { Product, ProductSummary } from '../../models';
import { MatIconModule } from '@angular/material/icon';
import { DialogService, ProductService } from '../../services';
import { Router, RouterModule } from '@angular/router';
import { FormatPricePipe } from '../../pipes/format-price.pipe';

@Component({
  selector: 'app-product-summary',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    FormatPricePipe,
    RouterModule,
  ],
  templateUrl: './product-summary.component.html',
  styleUrl: './product-summary.component.scss',
})
export class ProductSummaryComponent {
  @Input() product: ProductSummary = new Product();

  constructor(
    private productService: ProductService,
    private dialogService: DialogService,
    private router: Router
  ) {}
  addToCart() {
    if (!this.product.uKorpi)
      this.productService.addProductToCart(this.product.id!);
  }

  removeFromCart() {
    if (!this.product.uKorpi) return;
    this.dialogService
      .removeItemFromCartConfirmation()
      .afterClosed()
      .subscribe((isConfirmed) => {
        if (!isConfirmed) return;
        this.productService.removeProductFromCart(this.product.id!);
      });
  }

  getProductDetails() {
    this.router.navigate(['/proizvodi', this.product.id]);
  }
}
