import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogService, UserService } from '../../services';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { User, UserSummary } from '../../models';
import { Router } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-accounts-management',
  standalone: true,
  imports: [CommonModule, MatTableModule, MatButtonModule, MatIconModule],
  templateUrl: './accounts-management.component.html',
  styleUrl: './accounts-management.component.scss',
})
export class AccountsManagementComponent implements OnDestroy {
  displayedColumns: string[] = ['username', 'name', 'email', 'actions'];
  users: UserSummary[] = [];
  users$: Subscription;

  constructor(
    private userService: UserService,
    private dialogService: DialogService,
    private router: Router
  ) {
    this.users$ = this.userService.users$.subscribe((items) => {
      this.users = items;
    });
  }

  ngOnDestroy() {
    this.users$?.unsubscribe();
  }

  deleteAccount(event: any, user: UserSummary) {
    event.stopPropagation();
    this.dialogService
      .deleteAccountConfirmation()
      .afterClosed()
      .subscribe((isConfirmed: boolean) => {
        if (!isConfirmed) return;
        this.userService.deleteUser(user.id!);
      });
  }

  createAccount() {
    this.dialogService.createAccount('admin');
  }

  editAccount(event: any, user: UserSummary) {
    event.stopPropagation();

    this.dialogService
      .editAccount(user)
      .afterClosed()
      .subscribe((user: UserSummary) => {
        if (!user) return;
        this.userService.editUser(user);
      });
  }

  onRowSelected(data: UserSummary) {
    this.router.navigate(['/admin/korisnici', data.id]);
  }
}
