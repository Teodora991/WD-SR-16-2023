import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogService, ProductService } from '../../services';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { Product, ProductSummary } from '../../models';
import { Router } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-products-management',
  standalone: true,
  imports: [CommonModule, MatTableModule, MatButtonModule, MatIconModule],
  templateUrl: './products-management.component.html',
  styleUrl: './products-management.component.scss',
})
export class ProductsManagementComponent implements OnDestroy {
  displayedColumns: string[] = ['name', 'type', 'price', 'actions'];
  products: ProductSummary[] = [];
  products$: Subscription;
  constructor(
    private productService: ProductService,
    private dialogService: DialogService,
    private router: Router
  ) {
    this.products$ = this.productService.productItems$.subscribe((items) => {
      this.products = items;
    });
  }

  ngOnDestroy() {
    this.products$?.unsubscribe();
  }

  deleteProduct(event: any, product: ProductSummary) {
    event.stopPropagation();
    this.dialogService
      .deleteProductConfirmation()
      .afterClosed()
      .subscribe((isConfirmed: boolean) => {
        if (!isConfirmed) return;
        this.productService.deleteProduct(product.id!);
      });
  }

  editProduct(event: any, product: Product) {
    event.stopPropagation();
    this.dialogService
      .editProduct(product)
      .afterClosed()
      .subscribe((product: Product) => {
        if (!product) return;
        this.productService.editProduct(product);
      });
  }

  addProduct() {
    this.dialogService.createProduct();
  }

  onRowSelected(data: ProductSummary) {
    this.router.navigate(['/proizvodi', data.id]);
  }
}
