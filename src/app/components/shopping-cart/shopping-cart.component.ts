import { Component, OnDestroy } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { DialogService, ProductService } from '../../services';
import { ProductSummaryComponent } from '../product-summary/product-summary.component';
import { FormatPricePipe } from '../../pipes/format-price.pipe';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { ProductSummary } from '../../models';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-cart',
  standalone: true,
  imports: [
    CommonModule,
    ProductSummaryComponent,
    FormatPricePipe,
    MatButtonModule,
    RouterModule,
    MatListModule,
    MatIconModule,
    NgOptimizedImage,
    MatFormFieldModule,
    MatInputModule,
  ],
  templateUrl: './shopping-cart.component.html',
  styleUrl: './shopping-cart.component.scss',
})
export class ShoppingCartComponent implements OnDestroy {
  cartItems: ProductSummary[] = [];
  cartItems$: Subscription;
  constructor(
    public productService: ProductService,
    private dialogService: DialogService
  ) {
    this.cartItems$ = this.productService.productItems$.subscribe((items) => {
      this.cartItems = items.filter((item) => item.uKorpi);
    });
  }

  productsWithChangedQuantity: Map<string, number> = new Map();

  ngOnDestroy() {
    this.cartItems$?.unsubscribe();
  }

  removeFromCart(product: ProductSummary) {
    if (!product.uKorpi) return;
    this.dialogService
      .removeItemFromCartConfirmation()
      .afterClosed()
      .subscribe((isConfirmed) => {
        if (!isConfirmed) return;
        this.productService.removeProductFromCart(product.id!);
        this.productsWithChangedQuantity.delete(product.id!);
      });
  }

  decreaseQuantity(product: ProductSummary) {
    const initialQuanity =
      this.productsWithChangedQuantity.get(product.id!) || 1;
    this.productsWithChangedQuantity.set(product.id!, initialQuanity - 1 || 1);
  }

  increaseQuantity(product: ProductSummary) {
    const initialQuanity =
      this.productsWithChangedQuantity.get(product.id!) || 1;
    this.productsWithChangedQuantity.set(product.id!, initialQuanity + 1);
  }

  getQuantity(product: ProductSummary) {
    if (!this.productsWithChangedQuantity.has(product.id!)) return 1;

    return this.productsWithChangedQuantity.get(product.id!);
  }

  sendOrder() {
    this.productService.openSnackBar('Narudžbina uspešno završena!');
  }

  getTotalAmount() {
    let total = 0;

    this.cartItems.forEach((item) => {
      const quantity = this.productsWithChangedQuantity.get(item.id!) || 1;
      const price = item.cena! * quantity;
      total += price;
    });
    return total;
  }
}
