import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DialogService, ProductService } from '../../services';
import { Product, ProductSummary } from '../../models';
import { MatIconModule } from '@angular/material/icon';
import { StarRatingComponent } from '../star-rating/star-rating.component';
import { CarouselComponent } from '../carousel/carousel.component';
import { FormatPricePipe } from '../../pipes/format-price.pipe';

@Component({
  selector: 'app-product-details',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    StarRatingComponent,
    CarouselComponent,
    FormatPricePipe,
  ],
  templateUrl: './product-details.component.html',
  styleUrl: './product-details.component.scss',
})
export class ProductDetailsComponent implements OnDestroy {
  activeRoute$: Subscription;
  productData: any = new Product();
  selectedId: string | null = null;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private router: Router,
    private dialogService: DialogService
  ) {
    this.activeRoute$ = this.route.params.subscribe((params) => {
      this.selectedId = params['id'];
      this.productService
        .selectProductById(this.selectedId!)
        .subscribe((product) => {
          this.productData = product;
        });
    });
  }

  ngOnDestroy() {
    this.activeRoute$?.unsubscribe();
  }

  updateProductRating(rating: number) {
    const ratings = [...(this.productData.ocene || []), rating];
    const sum = ratings.reduce((total, rating) => total + rating, 0);
    const average = sum / ratings.length;
    this.productService.patchProduct(this.selectedId!, {
      ocene: ratings,
      prosecnaOcena: Number(average.toFixed(2)),
    });
  }

  addToCart() {
    if (!this.productData.uKorpi)
      this.productService.addProductToCart(this.selectedId!);
  }
}
