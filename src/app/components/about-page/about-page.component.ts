import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { PetShopService, ProductService } from '../../services';
import { MatCardModule } from '@angular/material/card';
import { MatDialogContent, MatDialogModule } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { PetShop, PetShopSummary } from '../../models';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-about-page',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatDialogContent,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  templateUrl: './about-page.component.html',
  styleUrl: './about-page.component.scss',
})
export class AboutPageComponent implements OnDestroy {
  petShop: PetShopSummary = new PetShop();
  petShop$: Subscription;
  products$: Subscription;
  images: string[] = [];
  mapProductTypeToImg: Map<string, string> = new Map();

  constructor(
    private petShopService: PetShopService,
    public productService: ProductService
  ) {
    this.petShopService.fetchStoreData();
    this.petShop$ = this.petShopService.petShop$.subscribe((petShop) => {
      this.petShop = petShop;
    });
    this.products$ = this.productService.productItems$.subscribe((products) => {
      products.forEach((product) => {
        this.images.push(...(product.slike ?? []));
        if (
          product.tip &&
          product.slike?.length &&
          !this.mapProductTypeToImg.has(product.tip)
        ) {
          this.mapProductTypeToImg.set(product.tip, product.slike[0]);
        }
      });
    });
  }

  sendComment() {
    this.productService.openSnackBar('Poslato!');
  }

  ngOnDestroy() {
    this.petShop$?.unsubscribe();
    this.products$?.unsubscribe();
  }
}
