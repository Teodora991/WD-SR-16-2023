import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductService } from '../../services';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { ProductSummaryComponent } from '../product-summary/product-summary.component';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ProductSummary } from '../../models';

@Component({
  selector: 'app-products-page',
  standalone: true,
  imports: [
    CommonModule,
    ProductSummaryComponent,
    MatGridListModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
  ],
  templateUrl: './products-page.component.html',
  styleUrl: './products-page.component.scss',
})
export class ProductsPageComponent implements OnDestroy {
  activeRoute$: Subscription;
  productsByCategory$: Subscription;
  filter: string = '';
  productsByCategory: ProductSummary[] = [];
  constructor(
    public productService: ProductService,
    private route: ActivatedRoute
  ) {
    this.activeRoute$ = this.route.queryParams.subscribe((params) => {
      this.filter = params['type'];
    });

    this.productsByCategory$ = this.productService.productItems$.subscribe(
      (products) => {
        if (this.filter) {
          this.productsByCategory = products.filter(
            (product) => product.tip === this.filter
          );
        } else {
          this.productsByCategory = products;
        }
      }
    );
  }

  ngOnDestroy() {
    this.activeRoute$?.unsubscribe();
    this.productsByCategory$?.unsubscribe();
  }
}
