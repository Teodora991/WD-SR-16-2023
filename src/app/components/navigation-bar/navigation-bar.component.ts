import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { PetShopService, DialogService, ProductService } from '../../services';
import { RouterModule } from '@angular/router';
import { MatBadgeModule } from '@angular/material/badge';
import { MatMenuModule } from '@angular/material/menu';
import { PetShop, PetShopSummary, ProductSummary } from '../../models';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-navigation-bar',
  standalone: true,
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    RouterModule,
    MatBadgeModule,
    MatMenuModule,
  ],
  templateUrl: './navigation-bar.component.html',
  styleUrl: './navigation-bar.component.scss',
})
export class NavigationBarComponent implements OnDestroy {
  petShop$: Subscription;
  cartItems$: Subscription;
  petShop: PetShopSummary = new PetShop();
  cartItems: ProductSummary[] = [];

  constructor(
    private dialogService: DialogService,
    private petShopService: PetShopService,
    private productService: ProductService
  ) {
    this.cartItems$ = this.productService.productItems$.subscribe((items) => {
      this.cartItems = items.filter((item) => item.uKorpi);
    });
    this.petShop$ = this.petShopService.petShop$.subscribe((petShop) => {
      this.petShop = petShop;
    });
  }

  ngOnDestroy() {
    this.petShop$?.unsubscribe();
    this.cartItems$?.unsubscribe();
  }

  onLogin(): void {
    this.dialogService.login();
  }

  editStore() {
    this.dialogService
      .editStore(this.petShop)
      .afterClosed()
      .subscribe((store: PetShop) => {
        if (!store) return;
        this.petShopService.editPetShop(store);
      });
  }
}
