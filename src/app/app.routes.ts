import { Routes } from '@angular/router';
import {
  AboutPageComponent,
  ProductDetailsComponent,
  ProductsPageComponent,
  ShoppingCartComponent,
  AccountDetailsComponent,
  AccountsManagementComponent,
  ProductsManagementComponent,
} from './components';

export const routes: Routes = [
  { path: 'proizvodi', component: ProductsPageComponent },
  { path: 'proizvodi/:id', component: ProductDetailsComponent },
  { path: 'admin/korisnici', component: AccountsManagementComponent },
  { path: 'admin/proizvodi', component: ProductsManagementComponent },
  { path: 'admin/korisnici/:id', component: AccountDetailsComponent },
  { path: 'korpa', component: ShoppingCartComponent },
  { path: 'o-nama', component: AboutPageComponent },
  { path: '**', redirectTo: 'proizvodi' },
];
