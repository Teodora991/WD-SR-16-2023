# PetShop Aplikacija

Dobrodošli u PetShop aplikaciju! Ova aplikacija se bavi prodajom opreme za kućne ljubimce i omogućava korisnicima da pregledaju proizvode, dodaju ih u korpu, i upravljaju svojim nalozima.

## Uputstvo za pokretanje

1. Klonirajte repozitorijum na svoj lokalni uređaj:

   ```bash
   git clone https://gitlab.com/Teodora991/pet-shop.git
   ```

2. Instalirajte zavisnosti koristeći npm (Node Package Manager):

   ```bash
   cd pet-shop
   npm install
   ```

3. Pokrenite aplikaciju:

   ```bash
   ng serve
   ```

4. Otvorite pregledač i pristupite aplikaciji na [http://localhost:4200/](http://localhost:4200/).

## Funkcionalnosti

### 1. Pregled korisnika

Klikom na link 'Korisnici' u okviru navigacije, pristupa se stranici koja sadrzi listu svih registrovanih korisnika, kao i opciju za dodavanje novog korisnika kao i brisanje postojeceg korisnickog naloga. Klikom na korisnika sa liste, pristupa se stranici koja sadrzi detalje o korisniku, kao i opcije za izmenu korisnickih podataka. Funkcionalosti dodavanja, imene i brisanja korisnika su u celosti implementirane.

### 2. O Nama stranica

Ova stranica sadrzi pregled svih podataka o prodavnici. Na dugme 'Izmeni' otvara se dijalog sa formom za izmenu podataka o prodavnici. Funkcionalnost izmene podataka o prodavnici je potpuno implementirana.

### 3. Pregled proizvoda

Klikom na link 'Proizvodi' u okviru navigacije, pristupa se stranici koja sadrzi širok spektar proizvoda za kućne ljubimce. Na ovoj stranici korisnici mogu dodati proizvod u korpu, kao i videti osnovne podatke o proizvodu. Ovde takodje postoji i opcija za dodavanje novog proizvoda. Klikom na dugme 'Više...' na pojedinacnom proizvodu, pristupa se stranici koja sadrzi detaljne podatke o proizvodu, kao i opcije za izmenu i brisanje proizvoda. Funkcionalosti dodavanja, izmene i brisanja proizvoda su u celosti implementirane.

### 4. Dodavanje proizvoda u korpu

Korisnici mogu dodavati proizvode u svoju korpu. Korpa sadrzi listu proizvoda koje je korisnik odabrao kao podatak o ukupnoj vrednosti proizvoda u korpi korisnika. Pored toga, korisnik moze svaki proizvod da ukloni iz korpe. Funkcionalosti dodavanja proizvoda u korpu i uklanjanja proizvoda iz korpe su u celosti implementirane.

### 5. Logovanje

Aplikacija omogućava korisnicima da se prijave na svoje naloge kao i da registruju nalog. Funkcionalnost registracije naloga je u celosti implementirana dok funkcionalosti logovanja u skladu sa specifikacijom nije implementirana.

## Tehnički detalji

Aplikacija je razvijena korišćenjem Angular framework-a, sa TypeScript-om za razvoj i Angular Material bibliotekom za stilizaciju.

## Autor

- Teodora Nedić SR 16/2023 ([@teodoranedic](https://github.com/Teodora991))

Hvala što koristite PetShop Aplikaciju! 🐾
